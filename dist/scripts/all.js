angular.module('app', [
  'ngRoute',
  'ngStorage',

  'app.controllers',
  'app.services'
])

.config(["$routeProvider", function ($routeProvider) {
  $routeProvider
    .when('/', {
      templateUrl: 'partials/home.html',
      controller: 'HomeCtrl'
    })
    .otherwise({
      redirectTo: '/'
    });
}])

;
angular.module('app.controllers', [])

.controller('HomeCtrl', ["$scope", "utils", "$timeout", "$http", function ($scope, utils, $timeout, $http) {
  $scope.progress = {
    max: parseInt($scope.$storage.number) || 0,
    value: 0
  };

  $scope.status = [
    "{code} \u2026 je le sens, c\u2019est le bon code pour ouvrir le #BFHCoffre ! http:\/\/www.lecoffrehardline.com  @BattlefieldEAFR",
    "On va voir si le #BFHCoffre s'ouvre comme \u201cshazam\u201d : {code} \u2026 ouvre toi ! http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Face au #BFHCoffre je n\u2019ai qu\u2019une seule chose \u00e0 dire : {code} ! http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Comme au loto mais en mieux : avec le code {code} j\u2019esp\u00e8re ouvrir LE #BFHCoffre http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "En direct de la salle des coffres, j\u2019essaie de trouver le bon code : {code} ? #BFHCoffre http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Les flics : OK. Les explosifs : OK. Le code du #BFHCoffre : {code} http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Pendant que les flics font la sieste, j\u2019essaie d\u2019ouvrir le #BFHCoffre avec le code {code} http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Si le code {code} n\u2019ouvre pas le coffre de #BFHCoffre, je passe aux explosifs http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Pourvu que {code} soit le bon code, j\u2019ai une r\u00e9putation d\u2019ouvreur de coffres \u00e0 tenir #BFHCoffre http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Ils passent le code de la route, moi le code pour le #BFHCoffre http:\/\/www.lecoffrehardline.com : peut-\u00eatre {code} ? @BattlefieldEAFR",
    "Alors, \u00e7a code ? Moi, j\u2019essaie d\u2019ouvrir le coffre de #BFHCoffre avec {code} http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Allez, je me lance : {code} pour ouvrir le #BFHCoffre http:\/\/www.lecoffrehardline.com ? @BattlefieldEAFR",
    "Entre le butin du #BFHCoffre et moi, il n\u2019y a qu\u2019un code : et si c\u2019\u00e9tait {code} ? http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Oh my code ! J\u2019essaie {code} pour ouvrir ce satan\u00e9 #BFHCoffre ! http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Quelqu\u2019un a le cheat code pour ouvrir le #BFHCoffre ? Non ? Bon, je tente {code} http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "J\u2019ajoute quatre, je retiens vingt \u2026 {code} est le code pour ouvrir le #BFHCoffre http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Ah ! J\u2019apprends que le #BFHCoffre r\u00e9siste au C4 mais pas aux codes ! {code} et ouvre toi ! http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Un voleur doit savoir voler avec un bon code : je parie que c\u2019est {code} ! #BFHCoffre http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Ouvre toi mon beau coffre, je suis sur que {code} est le bon code #BFHCoffre ! http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Dommage pour vous mais je pense que j'ouvrirai le #BFHCoffre avec {code} ! http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "3 guns, 8 grenades, un couteau et 2 bazookas... Moi je dis que le code du #BFHCoffre c'est {code} ! http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Allez, les flics vont bient\u00f4t arriver. Je tente le code {code} pour ouvrir le #BFHCoffre ! http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Mon surnom c'est Huggy les bons tuyaux, ce coffre est pour moi ! Testons {code} #BFHCoffre http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "J'ai braqu\u00e9 tellement de trucs que ce coffre c'est rien pour moi ! {code} est le code du #BFHCoffre http:\/\/www.lecoffrehardline.com @BattlefieldEAFR"
  ];

  $scope.queue = async.queue(function (task, nextTask) {
    $timeout(function () {
      task(nextTask);
    }, _.random($scope.$storage.minTime, $scope.$storage.maxTime));
  }, $scope.$storage.concurrency);

  $scope.queue.drain = function() {
    console.log('All items have been processed');
    alert('Done!');
  };

  $scope.$watch('progress.value', function () {
    var p = $scope.progress;
    p.percent = p.value ? Math.round(100 * p.value / p.max) : 0;
  });

  var tweet = function (message, next) {
    var url = 'https://twitter.com/i/tweet/create';

    var data = {
      authenticity_token: $scope.$storage.token,
      status: message
    };

    async.retry(999999, function (n) {
      $http.post(url + '?' + jQuery.param(data), null).success(function (res) {
        return next(null, res);
      }).error(n);
    }, next);
  };

  var spam = function () {
    if (!$scope.$storage.token) return console.log('No twitter token found');
    console.log('Spamming...');

    console.log('Preparing codes...');
    var codes = [],
        code, stringCode,
        min = parseInt($scope.$storage.min),
        num = parseInt($scope.$storage.number),
        clusPos = parseInt($scope.$storage.cluePos),
        clusNum = parseInt($scope.$storage.clueNum);

    for (var j = 0; j <= 9; j++) {
      for (var k = 0; k <= 9; k++) {
        for (var l = 0; l <= 9; l++) {
          for (var m = 0; m <= 9; m++) {
            code = [j.toString(), k.toString(), l.toString(), m.toString()];
            if (clusPos && clusNum) code[clusPos - 1] = clusNum;
            stringCode = code[0] + code[1] + code[2] + code[3];
            if (codes.indexOf(stringCode) < 0 && parseInt(stringCode) >= min) codes.push(stringCode);
          }
        }
      }
    }

    console.log('Preparing tasks...');
    async.each(_.take(codes, num), function (code, nextCode) {
      $scope.queue.push(function (n) {
        tweet(_.sample($scope.status).replace('{code}', code), function () {
          $scope.progress.value++;
          return n();
        });
      });

      return nextCode();
    });
  };

  $scope.go = function () {
    $scope.progress.value = 0;
    $scope.progress.max = parseInt($scope.$storage.number);

    utils.tab({
      query: 'https://*.twitter.com/*',
      url: 'https://www.twitter.com'
    }, function (tab, created) {
      var sendMessage = function () {
        console.log('Send message...');
        chrome.tabs.sendMessage(tab.id, {
          action: 'getToken'
        }, function (token) {
          if (token) {
            $scope.$storage.token = token;
            $scope.$apply();
            spam();
          }
        });
      };

      if (created) {
        console.log('Loading...');
        $timeout(function () {
          sendMessage();
          chrome.tabs.remove(tab.id);
        }, 2000);
      }
      else sendMessage();
    });
  };
}])

;
angular.module('app')

.run(["$rootScope", function ($rootScope) {
  $rootScope.$on('$routeChangeStart', function (event, currRoute) {
    $rootScope.currRoute = currRoute;
  });
}])

.run(["$rootScope", "$localStorage", function ($rootScope, $localStorage) {
  $rootScope.$storage = $localStorage.$default({
    token: '',
    min: 0,
    max: 9999,
    concurrency: 1,
    minTime: 2000,
    maxTime: 50000
  });
}])

;
angular.module('app.services', [])

.service('utils', function () {
  var _this = this;

  _this.tab = function (params, next) {
    chrome.tabs.query({
      url: params.query
    }, function (tabs) {
      var tab = tabs && tabs[0] ? tabs[0] : null;

      if (!tab) {
        chrome.tabs.create({
          url: params.url,
          active: params.active || false
        }, function (tab) {
          if (typeof next === 'function') return next(tab, true);
        });
      }
      else if (typeof next === 'function') return next(tab, false);
    });
  };
})

;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5qcyIsImNvbnRyb2xsZXJzLmpzIiwicnVuLmpzIiwic2VydmljZXMuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ25CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUN6SUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ25CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJhbGwuanMiLCJzb3VyY2VzQ29udGVudCI6WyJhbmd1bGFyLm1vZHVsZSgnYXBwJywgW1xuICAnbmdSb3V0ZScsXG4gICduZ1N0b3JhZ2UnLFxuXG4gICdhcHAuY29udHJvbGxlcnMnLFxuICAnYXBwLnNlcnZpY2VzJ1xuXSlcblxuLmNvbmZpZyhbXCIkcm91dGVQcm92aWRlclwiLCBmdW5jdGlvbiAoJHJvdXRlUHJvdmlkZXIpIHtcbiAgJHJvdXRlUHJvdmlkZXJcbiAgICAud2hlbignLycsIHtcbiAgICAgIHRlbXBsYXRlVXJsOiAncGFydGlhbHMvaG9tZS5odG1sJyxcbiAgICAgIGNvbnRyb2xsZXI6ICdIb21lQ3RybCdcbiAgICB9KVxuICAgIC5vdGhlcndpc2Uoe1xuICAgICAgcmVkaXJlY3RUbzogJy8nXG4gICAgfSk7XG59XSlcblxuOyIsImFuZ3VsYXIubW9kdWxlKCdhcHAuY29udHJvbGxlcnMnLCBbXSlcblxuLmNvbnRyb2xsZXIoJ0hvbWVDdHJsJywgW1wiJHNjb3BlXCIsIFwidXRpbHNcIiwgXCIkdGltZW91dFwiLCBcIiRodHRwXCIsIGZ1bmN0aW9uICgkc2NvcGUsIHV0aWxzLCAkdGltZW91dCwgJGh0dHApIHtcbiAgJHNjb3BlLnByb2dyZXNzID0ge1xuICAgIG1heDogcGFyc2VJbnQoJHNjb3BlLiRzdG9yYWdlLm51bWJlcikgfHwgMCxcbiAgICB2YWx1ZTogMFxuICB9O1xuXG4gICRzY29wZS5zdGF0dXMgPSBbXG4gICAgXCJ7Y29kZX0gXFx1MjAyNiBqZSBsZSBzZW5zLCBjXFx1MjAxOWVzdCBsZSBib24gY29kZSBwb3VyIG91dnJpciBsZSAjQkZIQ29mZnJlICEgaHR0cDpcXC9cXC93d3cubGVjb2ZmcmVoYXJkbGluZS5jb20gIEBCYXR0bGVmaWVsZEVBRlJcIixcbiAgICBcIk9uIHZhIHZvaXIgc2kgbGUgI0JGSENvZmZyZSBzJ291dnJlIGNvbW1lIFxcdTIwMWNzaGF6YW1cXHUyMDFkIDoge2NvZGV9IFxcdTIwMjYgb3V2cmUgdG9pICEgaHR0cDpcXC9cXC93d3cubGVjb2ZmcmVoYXJkbGluZS5jb20gQEJhdHRsZWZpZWxkRUFGUlwiLFxuICAgIFwiRmFjZSBhdSAjQkZIQ29mZnJlIGplIG5cXHUyMDE5YWkgcXVcXHUyMDE5dW5lIHNldWxlIGNob3NlIFxcdTAwZTAgZGlyZSA6IHtjb2RlfSAhIGh0dHA6XFwvXFwvd3d3LmxlY29mZnJlaGFyZGxpbmUuY29tIEBCYXR0bGVmaWVsZEVBRlJcIixcbiAgICBcIkNvbW1lIGF1IGxvdG8gbWFpcyBlbiBtaWV1eCA6IGF2ZWMgbGUgY29kZSB7Y29kZX0galxcdTIwMTllc3BcXHUwMGU4cmUgb3V2cmlyIExFICNCRkhDb2ZmcmUgaHR0cDpcXC9cXC93d3cubGVjb2ZmcmVoYXJkbGluZS5jb20gQEJhdHRsZWZpZWxkRUFGUlwiLFxuICAgIFwiRW4gZGlyZWN0IGRlIGxhIHNhbGxlIGRlcyBjb2ZmcmVzLCBqXFx1MjAxOWVzc2FpZSBkZSB0cm91dmVyIGxlIGJvbiBjb2RlIDoge2NvZGV9ID8gI0JGSENvZmZyZSBodHRwOlxcL1xcL3d3dy5sZWNvZmZyZWhhcmRsaW5lLmNvbSBAQmF0dGxlZmllbGRFQUZSXCIsXG4gICAgXCJMZXMgZmxpY3MgOiBPSy4gTGVzIGV4cGxvc2lmcyA6IE9LLiBMZSBjb2RlIGR1ICNCRkhDb2ZmcmUgOiB7Y29kZX0gaHR0cDpcXC9cXC93d3cubGVjb2ZmcmVoYXJkbGluZS5jb20gQEJhdHRsZWZpZWxkRUFGUlwiLFxuICAgIFwiUGVuZGFudCBxdWUgbGVzIGZsaWNzIGZvbnQgbGEgc2llc3RlLCBqXFx1MjAxOWVzc2FpZSBkXFx1MjAxOW91dnJpciBsZSAjQkZIQ29mZnJlIGF2ZWMgbGUgY29kZSB7Y29kZX0gaHR0cDpcXC9cXC93d3cubGVjb2ZmcmVoYXJkbGluZS5jb20gQEJhdHRsZWZpZWxkRUFGUlwiLFxuICAgIFwiU2kgbGUgY29kZSB7Y29kZX0gblxcdTIwMTlvdXZyZSBwYXMgbGUgY29mZnJlIGRlICNCRkhDb2ZmcmUsIGplIHBhc3NlIGF1eCBleHBsb3NpZnMgaHR0cDpcXC9cXC93d3cubGVjb2ZmcmVoYXJkbGluZS5jb20gQEJhdHRsZWZpZWxkRUFGUlwiLFxuICAgIFwiUG91cnZ1IHF1ZSB7Y29kZX0gc29pdCBsZSBib24gY29kZSwgalxcdTIwMTlhaSB1bmUgclxcdTAwZTlwdXRhdGlvbiBkXFx1MjAxOW91dnJldXIgZGUgY29mZnJlcyBcXHUwMGUwIHRlbmlyICNCRkhDb2ZmcmUgaHR0cDpcXC9cXC93d3cubGVjb2ZmcmVoYXJkbGluZS5jb20gQEJhdHRsZWZpZWxkRUFGUlwiLFxuICAgIFwiSWxzIHBhc3NlbnQgbGUgY29kZSBkZSBsYSByb3V0ZSwgbW9pIGxlIGNvZGUgcG91ciBsZSAjQkZIQ29mZnJlIGh0dHA6XFwvXFwvd3d3LmxlY29mZnJlaGFyZGxpbmUuY29tIDogcGV1dC1cXHUwMGVhdHJlIHtjb2RlfSA/IEBCYXR0bGVmaWVsZEVBRlJcIixcbiAgICBcIkFsb3JzLCBcXHUwMGU3YSBjb2RlID8gTW9pLCBqXFx1MjAxOWVzc2FpZSBkXFx1MjAxOW91dnJpciBsZSBjb2ZmcmUgZGUgI0JGSENvZmZyZSBhdmVjIHtjb2RlfSBodHRwOlxcL1xcL3d3dy5sZWNvZmZyZWhhcmRsaW5lLmNvbSBAQmF0dGxlZmllbGRFQUZSXCIsXG4gICAgXCJBbGxleiwgamUgbWUgbGFuY2UgOiB7Y29kZX0gcG91ciBvdXZyaXIgbGUgI0JGSENvZmZyZSBodHRwOlxcL1xcL3d3dy5sZWNvZmZyZWhhcmRsaW5lLmNvbSA/IEBCYXR0bGVmaWVsZEVBRlJcIixcbiAgICBcIkVudHJlIGxlIGJ1dGluIGR1ICNCRkhDb2ZmcmUgZXQgbW9pLCBpbCBuXFx1MjAxOXkgYSBxdVxcdTIwMTl1biBjb2RlIDogZXQgc2kgY1xcdTIwMTlcXHUwMGU5dGFpdCB7Y29kZX0gPyBodHRwOlxcL1xcL3d3dy5sZWNvZmZyZWhhcmRsaW5lLmNvbSBAQmF0dGxlZmllbGRFQUZSXCIsXG4gICAgXCJPaCBteSBjb2RlICEgSlxcdTIwMTllc3NhaWUge2NvZGV9IHBvdXIgb3V2cmlyIGNlIHNhdGFuXFx1MDBlOSAjQkZIQ29mZnJlICEgaHR0cDpcXC9cXC93d3cubGVjb2ZmcmVoYXJkbGluZS5jb20gQEJhdHRsZWZpZWxkRUFGUlwiLFxuICAgIFwiUXVlbHF1XFx1MjAxOXVuIGEgbGUgY2hlYXQgY29kZSBwb3VyIG91dnJpciBsZSAjQkZIQ29mZnJlID8gTm9uID8gQm9uLCBqZSB0ZW50ZSB7Y29kZX0gaHR0cDpcXC9cXC93d3cubGVjb2ZmcmVoYXJkbGluZS5jb20gQEJhdHRsZWZpZWxkRUFGUlwiLFxuICAgIFwiSlxcdTIwMTlham91dGUgcXVhdHJlLCBqZSByZXRpZW5zIHZpbmd0IFxcdTIwMjYge2NvZGV9IGVzdCBsZSBjb2RlIHBvdXIgb3V2cmlyIGxlICNCRkhDb2ZmcmUgaHR0cDpcXC9cXC93d3cubGVjb2ZmcmVoYXJkbGluZS5jb20gQEJhdHRsZWZpZWxkRUFGUlwiLFxuICAgIFwiQWggISBKXFx1MjAxOWFwcHJlbmRzIHF1ZSBsZSAjQkZIQ29mZnJlIHJcXHUwMGU5c2lzdGUgYXUgQzQgbWFpcyBwYXMgYXV4IGNvZGVzICEge2NvZGV9IGV0IG91dnJlIHRvaSAhIGh0dHA6XFwvXFwvd3d3LmxlY29mZnJlaGFyZGxpbmUuY29tIEBCYXR0bGVmaWVsZEVBRlJcIixcbiAgICBcIlVuIHZvbGV1ciBkb2l0IHNhdm9pciB2b2xlciBhdmVjIHVuIGJvbiBjb2RlIDogamUgcGFyaWUgcXVlIGNcXHUyMDE5ZXN0IHtjb2RlfSAhICNCRkhDb2ZmcmUgaHR0cDpcXC9cXC93d3cubGVjb2ZmcmVoYXJkbGluZS5jb20gQEJhdHRsZWZpZWxkRUFGUlwiLFxuICAgIFwiT3V2cmUgdG9pIG1vbiBiZWF1IGNvZmZyZSwgamUgc3VpcyBzdXIgcXVlIHtjb2RlfSBlc3QgbGUgYm9uIGNvZGUgI0JGSENvZmZyZSAhIGh0dHA6XFwvXFwvd3d3LmxlY29mZnJlaGFyZGxpbmUuY29tIEBCYXR0bGVmaWVsZEVBRlJcIixcbiAgICBcIkRvbW1hZ2UgcG91ciB2b3VzIG1haXMgamUgcGVuc2UgcXVlIGonb3V2cmlyYWkgbGUgI0JGSENvZmZyZSBhdmVjIHtjb2RlfSAhIGh0dHA6XFwvXFwvd3d3LmxlY29mZnJlaGFyZGxpbmUuY29tIEBCYXR0bGVmaWVsZEVBRlJcIixcbiAgICBcIjMgZ3VucywgOCBncmVuYWRlcywgdW4gY291dGVhdSBldCAyIGJhem9va2FzLi4uIE1vaSBqZSBkaXMgcXVlIGxlIGNvZGUgZHUgI0JGSENvZmZyZSBjJ2VzdCB7Y29kZX0gISBodHRwOlxcL1xcL3d3dy5sZWNvZmZyZWhhcmRsaW5lLmNvbSBAQmF0dGxlZmllbGRFQUZSXCIsXG4gICAgXCJBbGxleiwgbGVzIGZsaWNzIHZvbnQgYmllbnRcXHUwMGY0dCBhcnJpdmVyLiBKZSB0ZW50ZSBsZSBjb2RlIHtjb2RlfSBwb3VyIG91dnJpciBsZSAjQkZIQ29mZnJlICEgaHR0cDpcXC9cXC93d3cubGVjb2ZmcmVoYXJkbGluZS5jb20gQEJhdHRsZWZpZWxkRUFGUlwiLFxuICAgIFwiTW9uIHN1cm5vbSBjJ2VzdCBIdWdneSBsZXMgYm9ucyB0dXlhdXgsIGNlIGNvZmZyZSBlc3QgcG91ciBtb2kgISBUZXN0b25zIHtjb2RlfSAjQkZIQ29mZnJlIGh0dHA6XFwvXFwvd3d3LmxlY29mZnJlaGFyZGxpbmUuY29tIEBCYXR0bGVmaWVsZEVBRlJcIixcbiAgICBcIkonYWkgYnJhcXVcXHUwMGU5IHRlbGxlbWVudCBkZSB0cnVjcyBxdWUgY2UgY29mZnJlIGMnZXN0IHJpZW4gcG91ciBtb2kgISB7Y29kZX0gZXN0IGxlIGNvZGUgZHUgI0JGSENvZmZyZSBodHRwOlxcL1xcL3d3dy5sZWNvZmZyZWhhcmRsaW5lLmNvbSBAQmF0dGxlZmllbGRFQUZSXCJcbiAgXTtcblxuICAkc2NvcGUucXVldWUgPSBhc3luYy5xdWV1ZShmdW5jdGlvbiAodGFzaywgbmV4dFRhc2spIHtcbiAgICAkdGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICB0YXNrKG5leHRUYXNrKTtcbiAgICB9LCBfLnJhbmRvbSgkc2NvcGUuJHN0b3JhZ2UubWluVGltZSwgJHNjb3BlLiRzdG9yYWdlLm1heFRpbWUpKTtcbiAgfSwgJHNjb3BlLiRzdG9yYWdlLmNvbmN1cnJlbmN5KTtcblxuICAkc2NvcGUucXVldWUuZHJhaW4gPSBmdW5jdGlvbigpIHtcbiAgICBjb25zb2xlLmxvZygnQWxsIGl0ZW1zIGhhdmUgYmVlbiBwcm9jZXNzZWQnKTtcbiAgICBhbGVydCgnRG9uZSEnKTtcbiAgfTtcblxuICAkc2NvcGUuJHdhdGNoKCdwcm9ncmVzcy52YWx1ZScsIGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgcCA9ICRzY29wZS5wcm9ncmVzcztcbiAgICBwLnBlcmNlbnQgPSBwLnZhbHVlID8gTWF0aC5yb3VuZCgxMDAgKiBwLnZhbHVlIC8gcC5tYXgpIDogMDtcbiAgfSk7XG5cbiAgdmFyIHR3ZWV0ID0gZnVuY3Rpb24gKG1lc3NhZ2UsIG5leHQpIHtcbiAgICB2YXIgdXJsID0gJ2h0dHBzOi8vdHdpdHRlci5jb20vaS90d2VldC9jcmVhdGUnO1xuXG4gICAgdmFyIGRhdGEgPSB7XG4gICAgICBhdXRoZW50aWNpdHlfdG9rZW46ICRzY29wZS4kc3RvcmFnZS50b2tlbixcbiAgICAgIHN0YXR1czogbWVzc2FnZVxuICAgIH07XG5cbiAgICBhc3luYy5yZXRyeSg5OTk5OTksIGZ1bmN0aW9uIChuKSB7XG4gICAgICAkaHR0cC5wb3N0KHVybCArICc/JyArIGpRdWVyeS5wYXJhbShkYXRhKSwgbnVsbCkuc3VjY2VzcyhmdW5jdGlvbiAocmVzKSB7XG4gICAgICAgIHJldHVybiBuZXh0KG51bGwsIHJlcyk7XG4gICAgICB9KS5lcnJvcihuKTtcbiAgICB9LCBuZXh0KTtcbiAgfTtcblxuICB2YXIgc3BhbSA9IGZ1bmN0aW9uICgpIHtcbiAgICBpZiAoISRzY29wZS4kc3RvcmFnZS50b2tlbikgcmV0dXJuIGNvbnNvbGUubG9nKCdObyB0d2l0dGVyIHRva2VuIGZvdW5kJyk7XG4gICAgY29uc29sZS5sb2coJ1NwYW1taW5nLi4uJyk7XG5cbiAgICBjb25zb2xlLmxvZygnUHJlcGFyaW5nIGNvZGVzLi4uJyk7XG4gICAgdmFyIGNvZGVzID0gW10sXG4gICAgICAgIGNvZGUsIHN0cmluZ0NvZGUsXG4gICAgICAgIG1pbiA9IHBhcnNlSW50KCRzY29wZS4kc3RvcmFnZS5taW4pLFxuICAgICAgICBudW0gPSBwYXJzZUludCgkc2NvcGUuJHN0b3JhZ2UubnVtYmVyKSxcbiAgICAgICAgY2x1c1BvcyA9IHBhcnNlSW50KCRzY29wZS4kc3RvcmFnZS5jbHVlUG9zKSxcbiAgICAgICAgY2x1c051bSA9IHBhcnNlSW50KCRzY29wZS4kc3RvcmFnZS5jbHVlTnVtKTtcblxuICAgIGZvciAodmFyIGogPSAwOyBqIDw9IDk7IGorKykge1xuICAgICAgZm9yICh2YXIgayA9IDA7IGsgPD0gOTsgaysrKSB7XG4gICAgICAgIGZvciAodmFyIGwgPSAwOyBsIDw9IDk7IGwrKykge1xuICAgICAgICAgIGZvciAodmFyIG0gPSAwOyBtIDw9IDk7IG0rKykge1xuICAgICAgICAgICAgY29kZSA9IFtqLnRvU3RyaW5nKCksIGsudG9TdHJpbmcoKSwgbC50b1N0cmluZygpLCBtLnRvU3RyaW5nKCldO1xuICAgICAgICAgICAgaWYgKGNsdXNQb3MgJiYgY2x1c051bSkgY29kZVtjbHVzUG9zIC0gMV0gPSBjbHVzTnVtO1xuICAgICAgICAgICAgc3RyaW5nQ29kZSA9IGNvZGVbMF0gKyBjb2RlWzFdICsgY29kZVsyXSArIGNvZGVbM107XG4gICAgICAgICAgICBpZiAoY29kZXMuaW5kZXhPZihzdHJpbmdDb2RlKSA8IDAgJiYgcGFyc2VJbnQoc3RyaW5nQ29kZSkgPj0gbWluKSBjb2Rlcy5wdXNoKHN0cmluZ0NvZGUpO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIGNvbnNvbGUubG9nKCdQcmVwYXJpbmcgdGFza3MuLi4nKTtcbiAgICBhc3luYy5lYWNoKF8udGFrZShjb2RlcywgbnVtKSwgZnVuY3Rpb24gKGNvZGUsIG5leHRDb2RlKSB7XG4gICAgICAkc2NvcGUucXVldWUucHVzaChmdW5jdGlvbiAobikge1xuICAgICAgICB0d2VldChfLnNhbXBsZSgkc2NvcGUuc3RhdHVzKS5yZXBsYWNlKCd7Y29kZX0nLCBjb2RlKSwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICRzY29wZS5wcm9ncmVzcy52YWx1ZSsrO1xuICAgICAgICAgIHJldHVybiBuKCk7XG4gICAgICAgIH0pO1xuICAgICAgfSk7XG5cbiAgICAgIHJldHVybiBuZXh0Q29kZSgpO1xuICAgIH0pO1xuICB9O1xuXG4gICRzY29wZS5nbyA9IGZ1bmN0aW9uICgpIHtcbiAgICAkc2NvcGUucHJvZ3Jlc3MudmFsdWUgPSAwO1xuICAgICRzY29wZS5wcm9ncmVzcy5tYXggPSBwYXJzZUludCgkc2NvcGUuJHN0b3JhZ2UubnVtYmVyKTtcblxuICAgIHV0aWxzLnRhYih7XG4gICAgICBxdWVyeTogJ2h0dHBzOi8vKi50d2l0dGVyLmNvbS8qJyxcbiAgICAgIHVybDogJ2h0dHBzOi8vd3d3LnR3aXR0ZXIuY29tJ1xuICAgIH0sIGZ1bmN0aW9uICh0YWIsIGNyZWF0ZWQpIHtcbiAgICAgIHZhciBzZW5kTWVzc2FnZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgY29uc29sZS5sb2coJ1NlbmQgbWVzc2FnZS4uLicpO1xuICAgICAgICBjaHJvbWUudGFicy5zZW5kTWVzc2FnZSh0YWIuaWQsIHtcbiAgICAgICAgICBhY3Rpb246ICdnZXRUb2tlbidcbiAgICAgICAgfSwgZnVuY3Rpb24gKHRva2VuKSB7XG4gICAgICAgICAgaWYgKHRva2VuKSB7XG4gICAgICAgICAgICAkc2NvcGUuJHN0b3JhZ2UudG9rZW4gPSB0b2tlbjtcbiAgICAgICAgICAgICRzY29wZS4kYXBwbHkoKTtcbiAgICAgICAgICAgIHNwYW0oKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgfTtcblxuICAgICAgaWYgKGNyZWF0ZWQpIHtcbiAgICAgICAgY29uc29sZS5sb2coJ0xvYWRpbmcuLi4nKTtcbiAgICAgICAgJHRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHNlbmRNZXNzYWdlKCk7XG4gICAgICAgICAgY2hyb21lLnRhYnMucmVtb3ZlKHRhYi5pZCk7XG4gICAgICAgIH0sIDIwMDApO1xuICAgICAgfVxuICAgICAgZWxzZSBzZW5kTWVzc2FnZSgpO1xuICAgIH0pO1xuICB9O1xufV0pXG5cbjsiLCJhbmd1bGFyLm1vZHVsZSgnYXBwJylcblxuLnJ1bihbXCIkcm9vdFNjb3BlXCIsIGZ1bmN0aW9uICgkcm9vdFNjb3BlKSB7XG4gICRyb290U2NvcGUuJG9uKCckcm91dGVDaGFuZ2VTdGFydCcsIGZ1bmN0aW9uIChldmVudCwgY3VyclJvdXRlKSB7XG4gICAgJHJvb3RTY29wZS5jdXJyUm91dGUgPSBjdXJyUm91dGU7XG4gIH0pO1xufV0pXG5cbi5ydW4oW1wiJHJvb3RTY29wZVwiLCBcIiRsb2NhbFN0b3JhZ2VcIiwgZnVuY3Rpb24gKCRyb290U2NvcGUsICRsb2NhbFN0b3JhZ2UpIHtcbiAgJHJvb3RTY29wZS4kc3RvcmFnZSA9ICRsb2NhbFN0b3JhZ2UuJGRlZmF1bHQoe1xuICAgIHRva2VuOiAnJyxcbiAgICBtaW46IDAsXG4gICAgbWF4OiA5OTk5LFxuICAgIGNvbmN1cnJlbmN5OiAxLFxuICAgIG1pblRpbWU6IDIwMDAsXG4gICAgbWF4VGltZTogNTAwMDBcbiAgfSk7XG59XSlcblxuOyIsImFuZ3VsYXIubW9kdWxlKCdhcHAuc2VydmljZXMnLCBbXSlcblxuLnNlcnZpY2UoJ3V0aWxzJywgZnVuY3Rpb24gKCkge1xuICB2YXIgX3RoaXMgPSB0aGlzO1xuXG4gIF90aGlzLnRhYiA9IGZ1bmN0aW9uIChwYXJhbXMsIG5leHQpIHtcbiAgICBjaHJvbWUudGFicy5xdWVyeSh7XG4gICAgICB1cmw6IHBhcmFtcy5xdWVyeVxuICAgIH0sIGZ1bmN0aW9uICh0YWJzKSB7XG4gICAgICB2YXIgdGFiID0gdGFicyAmJiB0YWJzWzBdID8gdGFic1swXSA6IG51bGw7XG5cbiAgICAgIGlmICghdGFiKSB7XG4gICAgICAgIGNocm9tZS50YWJzLmNyZWF0ZSh7XG4gICAgICAgICAgdXJsOiBwYXJhbXMudXJsLFxuICAgICAgICAgIGFjdGl2ZTogcGFyYW1zLmFjdGl2ZSB8fCBmYWxzZVxuICAgICAgICB9LCBmdW5jdGlvbiAodGFiKSB7XG4gICAgICAgICAgaWYgKHR5cGVvZiBuZXh0ID09PSAnZnVuY3Rpb24nKSByZXR1cm4gbmV4dCh0YWIsIHRydWUpO1xuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICAgIGVsc2UgaWYgKHR5cGVvZiBuZXh0ID09PSAnZnVuY3Rpb24nKSByZXR1cm4gbmV4dCh0YWIsIGZhbHNlKTtcbiAgICB9KTtcbiAgfTtcbn0pXG5cbjsiXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=