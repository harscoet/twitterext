var focusOrCreateTab = function (url, next) {
  chrome.tabs.query({
    url: url
  }, function (tabs) {
    var tab = tabs && tabs[0] ? tabs[0] : null;

    if (tab) chrome.tabs.update(tab.id, { selected: true }, next);
    else chrome.tabs.create({ url: url, selected: true }, function (tab) {
      if (typeof next === 'function') return next(tab, true);
    });
  });
};

chrome.browserAction.onClicked.addListener(function () {
  focusOrCreateTab(chrome.extension.getURL('index.html'));
});