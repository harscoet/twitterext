angular.module('app.services', [])

.service('utils', function () {
  var _this = this;

  _this.tab = function (params, next) {
    chrome.tabs.query({
      url: params.query
    }, function (tabs) {
      var tab = tabs && tabs[0] ? tabs[0] : null;

      if (!tab) {
        chrome.tabs.create({
          url: params.url,
          active: params.active || false
        }, function (tab) {
          if (typeof next === 'function') return next(tab, true);
        });
      }
      else if (typeof next === 'function') return next(tab, false);
    });
  };
})

;