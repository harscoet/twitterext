angular.module('app', [
  'ngRoute',
  'ngStorage',

  'app.controllers',
  'app.services'
])

.config(function ($routeProvider) {
  $routeProvider
    .when('/', {
      templateUrl: 'partials/home.html',
      controller: 'HomeCtrl'
    })
    .otherwise({
      redirectTo: '/'
    });
})

;