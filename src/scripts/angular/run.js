angular.module('app')

.run(function ($rootScope) {
  $rootScope.$on('$routeChangeStart', function (event, currRoute) {
    $rootScope.currRoute = currRoute;
  });
})

.run(function ($rootScope, $localStorage) {
  $rootScope.$storage = $localStorage.$default({
    token: '',
    min: 0,
    max: 9999,
    concurrency: 1,
    minTime: 2000,
    maxTime: 50000
  });
})

;