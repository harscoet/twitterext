angular.module('app.controllers', [])

.controller('HomeCtrl', function ($scope, utils, $timeout, $http) {
  $scope.progress = {
    max: parseInt($scope.$storage.number) || 0,
    value: 0
  };

  $scope.status = [
    "{code} \u2026 je le sens, c\u2019est le bon code pour ouvrir le #BFHCoffre ! http:\/\/www.lecoffrehardline.com  @BattlefieldEAFR",
    "On va voir si le #BFHCoffre s'ouvre comme \u201cshazam\u201d : {code} \u2026 ouvre toi ! http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Face au #BFHCoffre je n\u2019ai qu\u2019une seule chose \u00e0 dire : {code} ! http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Comme au loto mais en mieux : avec le code {code} j\u2019esp\u00e8re ouvrir LE #BFHCoffre http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "En direct de la salle des coffres, j\u2019essaie de trouver le bon code : {code} ? #BFHCoffre http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Les flics : OK. Les explosifs : OK. Le code du #BFHCoffre : {code} http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Pendant que les flics font la sieste, j\u2019essaie d\u2019ouvrir le #BFHCoffre avec le code {code} http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Si le code {code} n\u2019ouvre pas le coffre de #BFHCoffre, je passe aux explosifs http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Pourvu que {code} soit le bon code, j\u2019ai une r\u00e9putation d\u2019ouvreur de coffres \u00e0 tenir #BFHCoffre http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Ils passent le code de la route, moi le code pour le #BFHCoffre http:\/\/www.lecoffrehardline.com : peut-\u00eatre {code} ? @BattlefieldEAFR",
    "Alors, \u00e7a code ? Moi, j\u2019essaie d\u2019ouvrir le coffre de #BFHCoffre avec {code} http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Allez, je me lance : {code} pour ouvrir le #BFHCoffre http:\/\/www.lecoffrehardline.com ? @BattlefieldEAFR",
    "Entre le butin du #BFHCoffre et moi, il n\u2019y a qu\u2019un code : et si c\u2019\u00e9tait {code} ? http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Oh my code ! J\u2019essaie {code} pour ouvrir ce satan\u00e9 #BFHCoffre ! http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Quelqu\u2019un a le cheat code pour ouvrir le #BFHCoffre ? Non ? Bon, je tente {code} http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "J\u2019ajoute quatre, je retiens vingt \u2026 {code} est le code pour ouvrir le #BFHCoffre http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Ah ! J\u2019apprends que le #BFHCoffre r\u00e9siste au C4 mais pas aux codes ! {code} et ouvre toi ! http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Un voleur doit savoir voler avec un bon code : je parie que c\u2019est {code} ! #BFHCoffre http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Ouvre toi mon beau coffre, je suis sur que {code} est le bon code #BFHCoffre ! http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Dommage pour vous mais je pense que j'ouvrirai le #BFHCoffre avec {code} ! http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "3 guns, 8 grenades, un couteau et 2 bazookas... Moi je dis que le code du #BFHCoffre c'est {code} ! http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Allez, les flics vont bient\u00f4t arriver. Je tente le code {code} pour ouvrir le #BFHCoffre ! http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "Mon surnom c'est Huggy les bons tuyaux, ce coffre est pour moi ! Testons {code} #BFHCoffre http:\/\/www.lecoffrehardline.com @BattlefieldEAFR",
    "J'ai braqu\u00e9 tellement de trucs que ce coffre c'est rien pour moi ! {code} est le code du #BFHCoffre http:\/\/www.lecoffrehardline.com @BattlefieldEAFR"
  ];

  $scope.queue = async.queue(function (task, nextTask) {
    $timeout(function () {
      task(nextTask);
    }, _.random($scope.$storage.minTime, $scope.$storage.maxTime));
  }, $scope.$storage.concurrency);

  $scope.queue.drain = function() {
    console.log('All items have been processed');
    alert('Done!');
  };

  $scope.$watch('progress.value', function () {
    var p = $scope.progress;
    p.percent = p.value ? Math.round(100 * p.value / p.max) : 0;
  });

  var tweet = function (message, next) {
    var url = 'https://twitter.com/i/tweet/create';

    var data = {
      authenticity_token: $scope.$storage.token,
      status: message
    };

    async.retry(999999, function (n) {
      $http.post(url + '?' + jQuery.param(data), null).success(function (res) {
        return next(null, res);
      }).error(n);
    }, next);
  };

  var spam = function () {
    if (!$scope.$storage.token) return console.log('No twitter token found');
    console.log('Spamming...');

    console.log('Preparing codes...');
    var codes = [],
        code, stringCode,
        min = parseInt($scope.$storage.min),
        num = parseInt($scope.$storage.number),
        clusPos = parseInt($scope.$storage.cluePos),
        clusNum = parseInt($scope.$storage.clueNum);

    for (var j = 0; j <= 9; j++) {
      for (var k = 0; k <= 9; k++) {
        for (var l = 0; l <= 9; l++) {
          for (var m = 0; m <= 9; m++) {
            code = [j.toString(), k.toString(), l.toString(), m.toString()];
            if (clusPos && clusNum) code[clusPos - 1] = clusNum;
            stringCode = code[0] + code[1] + code[2] + code[3];
            if (codes.indexOf(stringCode) < 0 && parseInt(stringCode) >= min) codes.push(stringCode);
          }
        }
      }
    }

    console.log('Preparing tasks...');
    async.each(_.take(codes, num), function (code, nextCode) {
      $scope.queue.push(function (n) {
        tweet(_.sample($scope.status).replace('{code}', code), function () {
          $scope.progress.value++;
          return n();
        });
      });

      return nextCode();
    });
  };

  $scope.go = function () {
    $scope.progress.value = 0;
    $scope.progress.max = parseInt($scope.$storage.number);

    utils.tab({
      query: 'https://*.twitter.com/*',
      url: 'https://www.twitter.com'
    }, function (tab, created) {
      var sendMessage = function () {
        console.log('Send message...');
        chrome.tabs.sendMessage(tab.id, {
          action: 'getToken'
        }, function (token) {
          if (token) {
            $scope.$storage.token = token;
            $scope.$apply();
            spam();
          }
        });
      };

      if (created) {
        console.log('Loading...');
        $timeout(function () {
          sendMessage();
          chrome.tabs.remove(tab.id);
        }, 2000);
      }
      else sendMessage();
    });
  };
})

;