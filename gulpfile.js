var gulp = require('gulp'),
    del = require('del'),
    runSequence = require('run-sequence'),
    p = require('gulp-load-plugins')(),
    argv = require('yargs').argv,
    mkdirp = require('mkdirp'),
    exec = require('child_process').exec,
    conf = require('./gulpconfig');

gulp.task('vendor-css', function () {
  gulp.src(conf.vendor.css)
    .pipe(p.concat('vendor.css'))
    .pipe(gulp.dest('dist/css'))
  ;
});

gulp.task('vendor-js', function () {
  gulp.src(conf.vendor.js)
    .pipe(p.concat('vendor.js'))
    .pipe(p.if(argv.prod, p.uglify({ mangle: false })))
    .pipe(gulp.dest('dist/scripts'))
  ;
});

gulp.task('vendor-fonts', function () {
  gulp.src(conf.vendor.fonts)
    .pipe(gulp.dest('dist/fonts'))
  ;
});

gulp.task('vendor-images', function () {
  gulp.src(conf.vendor.images)
    .pipe(gulp.dest('dist/images'))
  ;
});

gulp.task('vendor', function (callback) {
  runSequence(['vendor-css', 'vendor-js', 'vendor-fonts', 'vendor-images'], callback);
});

gulp.task('css', function () {
  gulp.src(conf.filters.scss)
    .pipe(p.compass({
      project: __dirname,
      css: 'dist/css',
      sass: 'src/compass',
      image: 'dist/images'
    }))
    .pipe(p.if(argv.watch, p.livereload({ auto: false })))
  ;
});

gulp.task('scripts-angular', function () {
  gulp.src(conf.filters.scripts.angular)
    .pipe(p.ngAnnotate())
    .pipe(p.sourcemaps.init())
      .pipe(p.concat('all.js'))
      .pipe(p.if(argv.prod, p.uglify()))
    .pipe(p.sourcemaps.write())
    .pipe(gulp.dest('dist/scripts'))
    .pipe(p.if(argv.watch, p.livereload({ auto: false })))
  ;
});

gulp.task('scripts-chrome', function () {
  gulp.src([conf.filters.scripts.chrome])
    .pipe(p.if(argv.prod, p.foreach(function (stream) {
      return stream.pipe(p.uglify());
    })))
    .pipe(gulp.dest('dist/scripts'))
    .pipe(p.if(argv.watch, p.livereload({ auto: false })))
  ;
});

gulp.task('clean', function (cb) {
  del(['dist'], cb);
});

gulp.task('manifest', function () {
  gulp.src(['manifest.json'])
    .pipe(gulp.dest('dist'))
  ;
});

gulp.task('templates', function () {
  gulp.src([conf.filters.views, '!src/views/components/*.jade'])
    .pipe(p.jade({
      locals: {
        watch: argv.watch
      }
    }))
    .pipe(gulp.dest('dist'))
    .pipe(p.if(argv.watch, p.livereload({ auto: false })))
  ;
});

gulp.task('images', function () {
  gulp.src(['src/images/**/*', '!src/images/icon.png'])
    .pipe(gulp.dest('dist/images'))
  ;
});

gulp.task('icons', function () {
  mkdirp('dist/images/icons', function () {
    ['16', '19', '38', '48', '128'].forEach(function (size) {
      exec('convert src/images/icon.png -resize ' + size + 'x' + size + ' dist/images/icons/icon-' + size + '.png');
    });
  });
});

gulp.task('bower-install', function () {
  return p.bower()
    .pipe(gulp.dest('bower_components/'))
  ;
});

gulp.task('watch', function () {
  p.livereload.listen();
  argv.watch = true;

  gulp.watch(conf.filters.manifest, ['manifest']);
  gulp.watch(conf.filters.scripts.angular, ['scripts-angular']);
  gulp.watch(conf.filters.scripts.chrome, ['scripts-chrome']);
  gulp.watch(conf.filters.scss, ['css']);
  gulp.watch(conf.filters.views, ['templates']);
});

gulp.task('scripts', function (callback) {
  runSequence(['scripts-angular', 'scripts-chrome'], callback);
});

gulp.task('install', function (callback) {
  runSequence('clean', ['manifest', 'css', 'scripts', 'templates', 'images', 'icons', 'vendor'], callback);
});

gulp.task('init', function (callback) {
  runSequence('bower-install', 'install', callback);
});

gulp.task('default', function (callback) {
  var tasks = argv.watch ? ['templates', 'scripts', 'css', 'watch'] : ['install'];

  runSequence(tasks, callback);
});